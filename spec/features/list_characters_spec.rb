require 'rails_helper'
require 'support/helpers/actions/log_in_helper'
require 'support/helpers/actions/sign_up_helper'

RSpec.configure do |c|
  c.include LogInHelper
  c.include SignUpHelper
end

RSpec.feature 'Heroes are listed', type: :feature, vcr: true do
  scenario 'Column titles are displayed' do
    sign_up

    expect(page).to have_text('Name')
    expect(page).to have_text('Description')
    expect(page).to have_text('Last updated')
  end

  scenario 'Heroes info are displayed inside table' do
    sign_up

    expect(page).to have_text('Abomination (Emil Blonsky)')
    expect(page).to have_text("Formerly known as Emil Blonsky, a spy of Soviet
    Yugoslavian origin working for the KGB, the Abomination gained his powers
    after receiving a dose of gamma radiation similar to that which transformed
    Bruce Banner into the incredible Hulk.")
  end
end
