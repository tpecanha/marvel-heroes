require 'rails_helper'

RSpec.feature 'Login Page is displayed', type: :feature do
  scenario 'Fields are displayed' do
    visit root_path

    fill_in 'marvel_api_auth_api_key', with: Rails.application.secrets.api_key
    fill_in 'marvel_api_auth_priv_key', with: Rails.application.secrets.priv_key

    expect(page).to have_text('Api key')
    expect(page).to have_text('Private key')
    expect(page).to have_text('Log in')
    expect(page).to have_button('Sign in')
    expect(page).to have_text('Do not have an account, click here!')
  end
end
