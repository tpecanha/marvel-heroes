require 'rails_helper'
# require 'database_cleaner'
require 'support/helpers/actions/sign_up_helper'

RSpec.configure do |c|
  c.include SignUpHelper
end

RSpec.feature 'Sign Up page is displayed', type: :feature, vcr: true do
  scenario 'Fields are displayed' do
    visit root_path

    click_link('Do not have an account, click here!')

    fill_in 'user_api_key', with: Rails.application.secrets.api_key
    fill_in 'user_priv_key', with: Rails.application.secrets.priv_key

    expect(page).to have_text('Api key')
    expect(page).to have_text('Private key')
    expect(page).to have_text('Sign Up')
    expect(page).to have_button('Submit')
    expect(page).to have_text('Do not have keys, click here!')
  end

  scenario 'Sign Up with valid data' do
    sign_up
  end

  scenario 'Sign Up with invalid data' do
    2.times { sign_up }

    expect(page).to have_text('The form contains 2 errors.')
    expect(page).to have_text('Api key has already been taken')
    expect(page).to have_text('Priv key has already been taken')

    # DatabaseCleaner.strategy = :truncation
    # DatabaseCleaner.clean
  end
end
