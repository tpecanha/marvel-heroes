FactoryGirl.define do
  factory :comic do
    title { FFaker::Movie.title }
    issue_number { 'A30' }
    thumbnail { File.new(File.join(Rails.root, 'spec/support', 'comic.jpg')) }
    description { FFaker::Lorem.phrase }
    marvel_id { 888 }
    # character
  end
end
