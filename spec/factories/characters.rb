FactoryGirl.define do
  factory :character do
    name { FFaker::Lorem.name }
    description { FFaker::Lorem.phrase }
    modified { FFaker::Time.date }
    resourceURI { FFaker::Internet.http_url }
    urls { [FFaker::Internet.http_url] }
    thumbnail { File.new(File.join(Rails.root, 'spec/support', 'marvel-funko-debut-animated-shorts-marvel-funko-short.jpg')) }
    marvel_id { 777 }
    # comic
  end
end
