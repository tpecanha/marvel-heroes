FactoryGirl.define do
  factory :user do
    priv_key { Rails.application.secrets.priv_key }
    api_key { Rails.application.secrets.api_key }
  end
end
