require 'rails_helper'

RSpec.describe MarvelApi, vcr: true do
  describe 'app/services/marvel_api.rb' do
    before(:each) do
      api_key = Rails.application.secrets.api_key
      priv_key = Rails.application.secrets.priv_key

      @marvel_api = MarvelApi.new(api_key: api_key, priv_key: priv_key)
    end
    describe 'GET #all_characters, validates' do
      let(:character_response) { @marvel_api.all_characters }
      context 'List All Character, after Authenticate in Marvels API' do
        it { expect(character_response.code).to eq(200) }
        it { expect(character_response.data[:total]).to be > 0 }
        it { expect(character_response.data[:results].first[:name]).to eq('3-D Man') }
        it { expect(character_response.data[:results].second[:description]).not_to eq(nil) }
        it { expect(character_response.data[:results].first[:id]).not_to eq(nil) }
        it { expect(character_response.data[:results].first[:thumbnail][:path]).not_to eq(nil) }
        it { expect(character_response.data[:count]).to eq(100) }
        it { expect(character_response.data[:limit]).to eq(100) }
      end

      context 'List Character Comics' do
        let(:comic_response) { @marvel_api.heroe_comics(marvel_id: 1_009_718) }
        it { expect(comic_response.code).to eq(200) }
        it { expect(comic_response.data[:total]).to be > 0 }
        it { expect(comic_response.data[:count]).to eq(20) }
        it { expect(comic_response.data[:limit]).to eq(20) }
        it { expect(comic_response.data[:results].first[:id]).not_to eq(nil) }
        it { expect(comic_response.data[:results].first[:title]).to eq('All-New X-Men (2015) #1.1') }
        it { expect(comic_response.data[:results].first[:issueNumber]).to eq(1.1) }
        it { expect(comic_response.data[:results].first[:description]).not_to eq(nil) }
        it { expect(comic_response.data[:results].first[:thumbnail]).not_to eq(nil) }
      end
    end
  end
end
