module LogInHelper
  def login
    visit root_path

    fill_in 'marvel_api_auth_api_key', with: Rails.application.secrets.api_key
    fill_in 'marvel_api_auth_priv_key', with: Rails.application.secrets.priv_key

    click_button('Sign in')
  end
end
