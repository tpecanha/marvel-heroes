module SignUpHelper
  def sign_up
    visit root_path

    click_link('Do not have an account, click here!')

    fill_in 'user_api_key', with: Rails.application.secrets.api_key
    fill_in 'user_priv_key', with: Rails.application.secrets.priv_key

    click_button('Submit')
  end
end
