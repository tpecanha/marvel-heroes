require 'rails_helper'

RSpec.describe 'app/models/characters_comic.rb', type: :model do
  describe CharactersComic, '#create, validates' do
    context 'A Character Comic' do
      it { is_expected.to belong_to(:character) }
      it { is_expected.to belong_to(:comic) }
      it { is_expected.to have_db_index(:character_id) }
      it { is_expected.to have_db_index(:comic_id) }
      # it { is_expected.to validate_uniqueness_of(:character_id).ignoring_case_sensitivity }
      # it { is_expected.to validate_uniqueness_of(:comic_id).ignoring_case_sensitivity }
    end
  end
end
