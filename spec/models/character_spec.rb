require 'rails_helper'

RSpec.describe 'app/models/character.rb', type: :model do
  before(:each) do
    @character = FactoryGirl.create :character
  end

  describe Character, '#create, validates' do
    context 'A Character' do
      subject { @character }
      it { is_expected.to respond_to(:name) }
      it { is_expected.to respond_to(:description) }
      it { is_expected.to respond_to(:modified) }
      it { is_expected.to respond_to(:resourceURI) }
      it { is_expected.to respond_to(:urls) }
      it { is_expected.to respond_to(:thumbnail) }
      it { is_expected.to respond_to(:name) }
      it { is_expected.to respond_to(:marvel_id) }
    end

  end


end
