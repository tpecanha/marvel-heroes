require 'rails_helper'

RSpec.describe 'app/models/user.rb', type: :model do
  before(:each) do
    @user = FactoryGirl.create :user
  end

  describe User, '#create, validates' do
    context 'A user' do
      subject { @user }
      it { is_expected.to respond_to(:priv_key) }
      it { is_expected.to respond_to(:api_key) }
      it { is_expected.to be_valid }
      it { is_expected.to validate_length_of(:priv_key).is_at_least(40).is_at_most(40) }
      it { is_expected.to validate_length_of(:api_key).is_at_least(32).is_at_most(32) }
      it { is_expected.to validate_uniqueness_of(:priv_key) }
      it { is_expected.to validate_uniqueness_of(:api_key) }
      it { is_expected.to validate_presence_of(:priv_key) }
      it { is_expected.to validate_presence_of(:api_key) }
    end
  end

end
