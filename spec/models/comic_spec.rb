require 'rails_helper'

RSpec.describe 'app/models/comic.rb', type: :model do
  before(:each) do
    @comic = FactoryGirl.create :comic
  end

  describe Comic, '#create, validates' do
    context 'A Comic' do
      subject { @comic }
      it { is_expected.to respond_to(:title) }
      it { is_expected.to respond_to(:issue_number) }
      it { is_expected.to respond_to(:thumbnail) }
      it { is_expected.to respond_to(:description) }
      it { is_expected.to respond_to(:marvel_id) }
    end
  end
end
