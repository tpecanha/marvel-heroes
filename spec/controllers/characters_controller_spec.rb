require 'rails_helper'

RSpec.describe CharactersController, type: :controller, vcr: true do
  describe 'app/controllers/characters_controller.rb' do
    describe '#index, validates' do
      let(:user) { FactoryGirl.create(:user) }
      context 'List All Character, after Authenticate in Marvels API' do
        before(:each) do
          allow(controller).to receive(:current_user) { user }
          get :index, params: {}
        end
        context 'Marvel characters are listed' do
          it { expect(response).to be_success }
        end
      end
    end
  end
end
