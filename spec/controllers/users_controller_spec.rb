require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  describe 'app/controllers/users_controller.rb' do
    let(:valid_session) {}

    describe 'GET #new User, validates' do
      before(:each) do
        get :new
      end
      context 'User sign up form is instantiated' do
        it { expect(response).to be_success }
      end
    end

    describe 'GET #show User, validates' do
      before(:each) do
        user = FactoryGirl.create(:user)
        get :show, params: { id: user }
      end
      context 'Show action is implemented' do
        it { expect(response).to be_success }
      end
    end

    describe 'POST #create User, validates' do
      before(:each) do
        user_attr = FactoryGirl.attributes_for(:user)
        post :create, params: { user: user_attr },
                      session: valid_session
      end
      context 'Marvel API credentials input form, returns OK response' do
        it { expect(response).to be_redirect }
      end
    end
  end
end
