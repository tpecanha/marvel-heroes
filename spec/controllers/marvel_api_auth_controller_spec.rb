require 'rails_helper'

RSpec.describe MarvelApiAuthController, type: :controller do
  describe 'app/controllers/marvel_api_auth_controller.rb' do
    let(:valid_session) {}

    describe 'GET #index, validates' do
      before(:each) do
        get :index, params: {}, session: valid_session
      end
      context 'Marvel API credentials input form, returns OK response' do
        it { expect(response).to be_success }
      end
    end

    describe 'POST #create, validates' do
      before(:each) do
        user = FactoryGirl.create(:user)
        get :create, params: { marvel_api_auth: { api_key: user.api_key } }
      end
      it { expect(response).to be_success }
    end
  end
end
