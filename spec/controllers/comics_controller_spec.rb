require 'rails_helper'

RSpec.describe ComicsController, type: :controller, vcr: true do
  describe 'app/controllers/comics_controller.rb' do
    let(:valid_session) {}
    describe '#index, validates' do
      let(:user) { FactoryGirl.create(:user) }
      context 'List Comics for the Heroe picked' do
        before(:each) do
          character = FactoryGirl.create(:character)
          allow(controller).to receive(:current_user) { user }
          get :index, params: { character_id: character.id }, session: valid_session
        end
        context 'Marvel comics are listed' do
          it { expect(response).to be_success }
        end
      end
    end
  end
end
