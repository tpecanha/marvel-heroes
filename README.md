# Marvel Heroes

*Marvel Heroes / Ruby on Rails 5*

## Overview

Marvel Heroes is an application to bring a little bit of the marvelous Marvel Heroe's universe.

It's developed using some of the best OOP concepts, lastest framework and gems versions, and almost full test's spec covered.

## Running the project

Dependencies
Ruby 2.3.1p112
Rails 5.0.0.1

Clone the project
```shell
git clone git@bitbucket.org:tpecanha/dp-marvel.git
```

Bundle it
```shell
bundle install
```

Setup the Database in SQlite3
```ruby
rails db:setup
```

Run the migrates to create the Database Schema
```ruby
rails db:migrate
```

Run the project
```ruby
rails s
```

## Running tests
```ruby
bundle exec rspec --format doc
```


# Known bugs

* When Character does not have comics, it must not be persisted on the database