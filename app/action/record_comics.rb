# TODO: Implement Cache
class RecordComics
  attr_reader :character_id

  def initialize(args)
    @character_id = args[:character_id]
  end

  def call(heroe_comics)
    comics = heroe_comics.data[:results].map do |comic|
      save_comic(comic)
    end
    comics
  end

  def save_comic(comic)
    character_comic = Comic.new
    character_comic.marvel_id = comic[:id]
    character_comic.title = comic[:title]
    character_comic.issue_number = comic[:issueNumber]
    character_comic.description = comic[:description]
    character_comic.thumbnail = comic[:thumbnail][:path]
    character_comic.save
    character = Character.find(character_id)
    character.comics << character_comic
  end
end
