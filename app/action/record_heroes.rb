# TODO: Implement cache

class RecordHeroes
  # Recording API characters on the database
  def self.call(heroes)
    characters = heroes.data[:results].map do |heroe|
      save_character(heroe)
    end
    characters
  end

  def self.save_character(heroe)
    character = Character.new
    character.marvel_id = heroe[:id]
    character.name = heroe[:name]
    character.description = heroe[:description]
    character.modified = heroe[:modified]
    character.resourceURI = heroe[:resourceURI]
    character.thumbnail = heroe[:thumbnail][:path]
    character.save
    character.comics.build
    character
  end
end
