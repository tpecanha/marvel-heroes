# Putting all the logic from views, inside a Decorator
class UserDecorator < SimpleDelegator
  attr_reader :user

  def initialize(user)
    @user = user
    __setobj__(user)
  end

  def eql?(other)
    target == self || user.eql?(other)
  end

  def errors?
    user.errors.any?
  end

  # def errors?
  #   if user.errors.any?
  #     helpers.content_tag(:div,
  #                         content_tag(:h4,
  #                                     "You got<%= pluralize(user.errors.count, 'error') %>",
  #                                     class: 'alert-heading'), content_tag(:ul, '<%= user.errors.full_messages.each do |msg| %>
  #                                                                                 <li><%= msg %></li>
  #                                                                                 <%= end %>
  #                                                                                 </ul>'),
  #                         class: 'alert alert-danger',
  #                         role: 'alert')
  #   end
  # end

  private

  def helpers
    ApplicationController.helpers
  end
end
