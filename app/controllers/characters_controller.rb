class CharactersController < ApplicationController
  def index
    all_characters.empty? ? fetch_characters : all_characters
    @characters = paginate
  end

  private

  def all_characters
    Character.all
  end

  def paginate
    Kaminari.paginate_array(all_characters).page(params[:page]).per(10)
  end

  def fetch_characters
    marvel_api = MarvelApi.new(api_key: current_user.api_key, priv_key: current_user.priv_key)
    heroes = marvel_api.all_characters
    RecordHeroes.call(heroes)
  end

  def character_params
    params.permit(:api_key, :priv_key)
  end
end
