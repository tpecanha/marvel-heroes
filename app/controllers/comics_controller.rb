class ComicsController < ApplicationController
  def index
    all_comics.empty? ? fetch_comics : all_comics
  end

  private

  def all_comics
    @comics = set_heroe.characters_comics.all
  end

  def fetch_comics
    marvel_api = MarvelApi.new(api_key: current_user.api_key, priv_key: current_user.priv_key)
    heroe_comics = marvel_api.heroe_comics(marvel_id: set_heroe.marvel_id)
    comic = RecordComics.new(character_id: set_heroe.id)
    comic.call(heroe_comics)
  end

  def set_heroe
    @heroe = Character.find(params[:character_id])
  end
end
