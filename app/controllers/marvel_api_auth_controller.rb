class MarvelApiAuthController < ApplicationController
  def index; end

  def create
    @user = finduser_by_apikey
    if @user && validate_private_key
      LogIn.call(@user, session)
      redirect_to characters_path
    else
      flash.now[:danger] = 'API key / Private key incorrect or not found'
      render 'index'
    end
  end

  private

  def finduser_by_apikey
    User.find_by(api_key: params[:marvel_api_auth][:api_key])
  end

  def validate_private_key
    @user.priv_key == params[:marvel_api_auth][:priv_key]
  end
end
