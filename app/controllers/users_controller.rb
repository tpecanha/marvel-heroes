class UsersController < ApplicationController
  before_action :set_user, only: [:show]

  def show; end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    respond_to do |format|
      if @user.save
        LogIn.call(@user, session)
        format.html { redirect_to characters_path, notice: 'NOW! You are one of our Heroes' }
        format.json { render json: @user }
      else
        format.html { render 'new' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:api_key, :priv_key)
  end
end
