class Character < ApplicationRecord
  validates_uniqueness_of :marvel_id

  has_many :characters_comics
  has_many :comics, through: :characters_comics
end
