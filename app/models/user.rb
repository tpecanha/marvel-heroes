class User < ApplicationRecord
  validates :api_key, :priv_key, presence: true
  validates :api_key, :priv_key, uniqueness: true
  validates :api_key, length: { in: 32..32 }
  validates :priv_key, length: { in: 40..40 }
end
