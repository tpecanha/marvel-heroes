class Comic < ApplicationRecord
  has_many :characters_comics
  has_many :comics, through: :characters_comics
end
