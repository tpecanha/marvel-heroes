class CharactersComic < ApplicationRecord
  belongs_to :character, dependent: :destroy
  belongs_to :comic, dependent: :destroy

  validates :character, presence: true
  validates :comic, presence: true
end
