# These are the parameters to send a GET request to the Marvel's API
# ts = TimeStamp(Time.now.to_s) + PUB_KEY + hash = Digest::MD5.hexdigest(ts + PRIV_KEY + PUB_KEY)
# http://gateway.marvel.com/v1/public/characters?ts=2016-11-29%2016:48:55%20-0200&apikey=<PUB_KEY>&hash=8448449e922dd5a2411c9675b336d7f7

# TODO: Exception handler
class MarvelApi
  attr_accessor :client, :characters

  def initialize(args)
    @client = Marvelite::API::Client.new(public_key: args[:api_key], private_key: args[:priv_key])
  end

  def all_characters
    client.characters(limit: 100)
  end

  def heroe_comics(args)
    client.character_comics(args[:marvel_id].to_i)
  end
end
