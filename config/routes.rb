Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'marvel_api_auth#index'
  post   '/login',      to: 'marvel_api_auth#create'
  delete '/logout',     to: 'marvel_api_auth#destroy'
  get    '/signup',     to: 'users#new'

  resources :users,           only: [:create, :show, :new]
  resources :characters,      only: [:index] do
    resources :comics,        only: [:index]
  end
  resources :comics, only: [:index]
end
