# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161206132348) do

  create_table "characters", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "modified"
    t.string   "resourceURI"
    t.string   "urls"
    t.string   "thumbnail"
    t.integer  "marvel_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "characters_comics", force: :cascade do |t|
    t.integer  "character_id"
    t.integer  "comic_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["character_id"], name: "index_characters_comics_on_character_id"
    t.index ["comic_id"], name: "index_characters_comics_on_comic_id"
  end

  create_table "comics", force: :cascade do |t|
    t.string   "title"
    t.float    "issue_number"
    t.string   "thumbnail"
    t.text     "description"
    t.integer  "marvel_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "priv_key"
    t.string   "api_key"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
