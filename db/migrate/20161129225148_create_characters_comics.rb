class CreateCharactersComics < ActiveRecord::Migration[5.0]
  def change
    create_table :characters_comics do |t|
      t.belongs_to :character, index: true
      t.belongs_to :comic, index: true
      t.timestamps
    end
  end
end
