class CreateComics < ActiveRecord::Migration[5.0]
  def change
    create_table :comics do |t|
      t.string :title
      t.float :issue_number
      t.string :thumbnail
      t.text :description
      t.integer :marvel_id
      t.timestamps
    end
  end
end
