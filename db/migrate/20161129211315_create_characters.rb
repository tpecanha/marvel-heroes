class CreateCharacters < ActiveRecord::Migration[5.0]
  def change
    create_table :characters do |t|
      t.string :name
      t.text :description
      t.datetime :modified
      t.string :resourceURI
      t.string :urls, array: true
      t.string :thumbnail
      t.integer :marvel_id
      t.timestamps
    end
  end
end
